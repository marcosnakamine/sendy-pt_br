# Sendy
Sistema de envio de newsletter

[Site oficial](https://sendy.co/)

---

## Versão
2.1.0.2

---

## Instalação

- Joque a pasta **pt_BR** na pasta **/locale**.
- Altere o idioma para pt_BR.

Clique [aqui](https://sendy.co/translation) para mais informações.

---

## Contribuir

- Baixe o programa [poedit](https://poedit.net/download)
- Abra o arquivo pt_BR/LC_MESSAGES/default.po
- Traduza =)

Ou envie uma [Issues](https://gitlab.com/marcosnakamine/sendy-pt_br/issues)